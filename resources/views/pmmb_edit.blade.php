@extends('layout')
@section('konten')
        <div class="card-body">
         <form action="{{url('pmmb/'.$collection[0]->id)}}" method="POST">
                        @csrf
                         @foreach($collection as $item)
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" name="_method" value="PUT">
                                <input type="hidden" name="id" id="id" value="{{$item->id}}">
                                <label for="nama">  Nama </label>
                                <input type="text" name="nama" id="nama" class="form-control" value="{{$item->nama}}">
                            </div>
                            <div class="form-group">
                                <label for="universitas">  Universitas</label>
                                <input type="text" name="universitas" id="universitas" class="form-control" value="{{$item->universitas}}">
                            </div>
                             <div class="form-group">
                                <label for="jurusan">  Jurusan</label>
                                <input type="text" name="jurusan" id="jurusan" class="form-control" value="{{$item->jurusan}}">
                            </div>
                                <button type="submit" name="save" id="save" class="btn btn-success" href="{{ url('pmmb') }}">Update</button>
                                 @endforeach
                        </form>
                </div>
@endsection
