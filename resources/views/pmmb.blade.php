@extends('layout')
@section('konten')
        
            <div class="content">
                
                 <div class="card-body">
                    
                 <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped " id="datatables">
                    <thead>
                    <tr>
                        <th>Nama</th>
                         <th>Universitas</th>
                          <th>Jurusan</th>
                        <th>Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($collection as $item)
                        <tr>
                            <td>{{$item->nama}}</td>
                            <td>{{$item->universitas}}</td>
                            <td>{{$item->jurusan}}</td>
                            <td>
                                <a href="{{url('pmmb/'.$item->id.'/edit')}}">
                                    <button type="button" class="btn btn-outline-primary">Edit</button>
                                </a>
                            <form class="" action="{{url('pmmb/'.$item->id)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                    <input type="submit" class="btn btn-outline-danger" value="Delete">
                            </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            </div>
        </div>
@endsection