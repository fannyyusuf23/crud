<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $collection=DB::table('pmmb_user')->get();
        return view('pmmb', ["collection"=>$collection]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pmmb_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data['nama']=$request->input('nama');
        $data['jurusan']=$request->input('jurusan');
        $data['universitas']=$request->input('universitas');

        DB::table('pmmb_user')->insert($data);
        return redirect('pmmb');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $collection = DB:: table('pmmb_user')->where('id',$id)->get();
        return view('pmmb_edit', ["collection"=>$collection]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         DB::table('pmmb_user')->where('id',$request->id)->update([
            'nama' => $request->nama,
            'jurusan' => $request->jurusan,
            'universitas' => $request->universitas
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('pmmb');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('pmmb_user')->where('id',$id)->delete();
        return redirect('pmmb');
    }
}
