<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tabel1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('pmmb_user', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('Nama');
            $table->string('Universitas');
            $table->string('Jurusan');
            $table->timestamps();
            $table->softDeletes();
            $table->string('delete_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
