@extends('layout')
@section('konten')
        <div class="card-body">
         <form action="{{url('pmmb')}}" method="POST">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="nama">  Nama </label>
                                <input type="text" name="nama" id="nama" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="universitas">  Universitas</label>
                                <input type="text" name="universitas" id="universitas" class="form-control">
                            </div>
                             <div class="form-group">
                                <label for="jurusan">  Jurusan</label>
                                <input type="text" name="jurusan" id="jurusan" class="form-control">
                            </div>
                                <button type="submit" name="save" id="save" class="btn btn-success" href="{{ url('pmmb') }}">Submit</button>
                        </form>
                </div>
@endsection